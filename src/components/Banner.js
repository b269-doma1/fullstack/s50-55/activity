
/*import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';*/

import {Button, Row, Col} from 'react-bootstrap';
import { NavLink } from "react-router-dom"




export default function Banner() {
return (
    <Row>
    	<Col className="p-5">
            <h1>Zuitt Coding Bootcamp</h1>
            <p>Opportunities for everyone, everywhere.</p>
            <NavLink to = "/error">
            <Button variant="primary">Enroll now!</Button>
            </NavLink>
        </Col>
    </Row>
	)
}