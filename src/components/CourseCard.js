

// Activity

/*import {Button, Card} from 'react-bootstrap';

export default function CourseCard() {
  return (
    <Card>
        <Card.Body>
	        <Card.Title>Sample Course</Card.Title>
	        <Card.Text className="mb-0">Description </Card.Text>
	        <Card.Text>This is a sample course offering</Card.Text>
	        <Card.Text className="mb-0">Price:</Card.Text>
	        <Card.Text>PhP 40,000</Card.Text>

        <Button variant="primary">Enroll</Button>
      </Card.Body>
    </Card>
  );
}*/

import {useState, useEffect} from 'react';

import {Link} from 'react-router-dom';

import { Button, Row, Col, Card } from 'react-bootstrap';


export default function CourseCard({course}) {

	const { name, description, price, _id } = course;

    /*
    SYNTAX:
        const [getter, setter] = useState(initialGetterVlue)
    */

//     const [count, setCount] = useState(0);
//     const [seats, setSeats] = useState(5);


//   /*  function enroll () {
//         // setCount(count + 1)
// // ACTIVITY
//         if (count < 30) {
//             setCount(count + 1);

//         } else {
//             alert('no more seats')
//             setCount(0);
//         }
//     };*/

//     function enroll () {
//   /*      if (seats > 0) {
//             setCount(count + 1);
//             console.log('Enrollees' + count);
//             setSeats(seats - 1);
//             console.log('Seats' + seats);
//         } else {
//             alert('no more seats available')
//         }*/

//         setCount(count + 1);
//         setSeats(seats - 1);
//     };

// // useEfect
// useEffect(() => {
//     if(seats <= 0) {
//         alert("No more seats available!")
//     }
// }, [seats]);

return (
    <Row className="mt-3 mb-3">
        <Col xs={12}>
            <Card className="cardHighlight p-0">
                <Card.Body>
                    <Card.Title><h4>{name}</h4></Card.Title>
                    <Card.Subtitle>Description</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price</Card.Subtitle>
                    <Card.Text>{price}</Card.Text>

                    {/*<Card.Subtitle>Count: {count}</Card.Subtitle>
                    <Card.Subtitle>Seats: {seats}</Card.Subtitle>*/}

                    {/*<Button variant="primary" onClick={enroll} disabled = {seats<=0}>Enroll</Button>*/}

                    <Button className = "bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
                </Card.Body>
            </Card>
        </Col>
    </Row>        
    )
}

