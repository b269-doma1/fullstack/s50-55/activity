
import {Button} from 'react-bootstrap';
import { NavLink } from "react-router-dom"


export default function Error() {

	return (
		<>
			<h1>Error 404 - Page not found.</h1>
			<p>The page you are looking for cannot be found</p>

			<NavLink to = "/">
	        	<Button variant="primary">Back to Home!</Button>
	        </NavLink>	
        </>
	)
		
	
}