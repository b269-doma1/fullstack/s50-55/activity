
import {useState, useEffect} from 'react';

// import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses() {

	// to store the courses retrieved from the database
	const [courses, setCourses] = useState([]);

	// console.log(coursesData);

	// const courses = coursesData.map(course => {
	// 	return (
	// 		< CourseCard key={course.id} course = {course} />
	// 	)
	// })

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setCourses(data.map(course => {
				return(
					<CourseCard key={course._id} course={course} />
				)
			}))
		})
	}, [])

	return (
		<>
		{courses}
		</>
	)
}


// Props
/*way to declare props drilling*/
/*we can pass information from one component to aother using props (props drilling)*/
/*curly braces {} are used for props to signify that we are providing passing information from one component to another using JS expression*/