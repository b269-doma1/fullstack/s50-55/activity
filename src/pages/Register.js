

import {useState, useEffect, useContext} from 'react';

import { Form, Button } from 'react-bootstrap';

// Activity
import {Navigate} from 'react-router-dom';

import UserContext from '../UserContext';


export default function Register() {

    const {user} = useContext(UserContext);


    // to store and manage value of the input fields

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [mobileNumber, setMobileNumber] = useState("");

    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        if(( email !== "" && password1 !== "" && password2 !=="") && password1 === password2) {
            setIsActive(true);
        } else {
            setIsActive(false);
        };
    }, [email, password1, password2]);

    // function to simulate user registration
    function registerUser(e) {
        e.preventDefault();

        // Clear input fields
        setFirstName("")
        setLastName("")
        setMobileNumber("")
        setEmail("");
        setPassword1("");
        setPassword2("");

        alert("Thank you for registering!");
    };

    return (
        // activity
        (user.id !== null) ?
        <Navigate to = "/courses"/>
        :
       
        <Form onSubmit={(e) => registerUser(e)} >

            <Form.Group controlId="userFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="firstName" 
                    placeholder="Enter first Name" 
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your First Name with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="userLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="lastName" 
                    placeholder="Enter last Name" 
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your Last Name with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="userMobileNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="mobileNumber" 
                    placeholder="Enter mobile Number" 
                    value={mobileNumber}
                    onChange={e => setMobileNumber(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your Mobile Number with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    required
                />
            </Form.Group>

            {isActive ?
                <Button variant="primary" type="submit" id="submitBtn">
                Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                Submit
                </Button>
            }
            
        </Form>
       
    )

}
